package config;

import config.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	  //用于文件上传
//    @Value("${file.diskPath}")
//    private String diskPath;  
	
	/**
	 * 拦截资源
	 */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/swagger-ui.html","/upload/**")
                .excludePathPatterns("/penRecordInfo/**")
                .excludePathPatterns("/penTemplate/**")
                .excludePathPatterns("/chat/**")
                .excludePathPatterns("/pdfReport/**")
                .excludePathPatterns("/interview/**")
                .excludePathPatterns("/interviewMonitoring/**")
                .excludePathPatterns("/interviewSpeaker/**")
                .excludePathPatterns("/swagger-resources/**", "/webjars/**","/error/**", "/v2/**","/swagger-ui.html/**");

    }

    /**
     * 
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/upload/**").addResourceLocations("file:"+diskPath+"/");
//        registry.addResourceHandler("swagger-ui.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//        registry.addResourceHandler("/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
