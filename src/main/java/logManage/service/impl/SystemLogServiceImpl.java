package logManage.service.impl;

import cn.hutool.json.JSONObject;
import logManage.entity.SystemLog;
import logManage.mapper.SystemLogMapper;
import logManage.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Title: LogServiceImpl
 * Description: 系统日志实现类
 * @author ggkt
 *
 */
@Service
public class SystemLogServiceImpl implements SystemLogService {

	@Autowired
	SystemLogMapper logMapper;
	@Override
	public int insertLog(SystemLog systemLog) {
		return logMapper.insertLog(systemLog);
	}

	@Override
	public List<JSONObject> listPage(SystemLog systemLog) {
		return logMapper.listPage(systemLog);
	}

}
