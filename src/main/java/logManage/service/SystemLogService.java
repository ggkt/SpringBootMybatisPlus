package logManage.service;

import cn.hutool.json.JSONObject;
import logManage.entity.SystemLog;

import java.util.List;

/**
 * Title: LogService
 * Description:系统日志接口类 
 * @author ggkt
 *
 */
public interface SystemLogService {

	int insertLog(SystemLog systemLog);

    List<JSONObject> listPage(SystemLog systemLog);
}
