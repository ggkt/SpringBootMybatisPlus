package logManage.controller;

import com.ggkt.common.utils.PageBean;
import com.ggkt.common.utils.web.RestResult;
import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import config.annotation.Log;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import logManage.entity.SystemLog;
import logManage.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Title: LogController
 * Description: 系统日志控制类
 * @author ggkt
 *
 */
@RestController
@RequestMapping("/systemLog")
@Api(tags = "系统日志")
public class SystemLogController {
    @Autowired
    SystemLogService systemLogService;

    @GetMapping("/list")
    @Log("系统日志")
    @ApiOperation("查询系统日志")
    public RestResult list(SystemLog systemLog){
        Page<Object> page = PageMethod.startPage(systemLog.getCurrentPage(), systemLog.getPageSize());
        systemLogService.listPage(systemLog);
        return RestResult.success(new PageBean<>(page));
    }

}
