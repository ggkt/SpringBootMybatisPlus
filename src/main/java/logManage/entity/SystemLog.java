package logManage.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ggkt.common.utils.dao.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;

/**
 * Title: Log
 * Description: 系统日志类
 * @author ggkt
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "日志表实体")
public class SystemLog extends Query {
	@ApiModelProperty(value = "日志表主键")
	private Integer logID;

	@ApiModelProperty(value = "adminID")
	private Integer adminID;

	@ApiModelProperty(value = "操作时间")
	private Date operateTime;

	@ApiModelProperty(value = "登陆时间")
	private Date loginTime;

	@ApiModelProperty(value ="登出时间")
	private Date signOutTime;

	@ApiModelProperty(value = "当日累计登录时长")
	private Integer totalTime;

	@ApiModelProperty(value = "登录IP")
	private String loginIp;

	@ApiModelProperty(value = "操作记录")
	private String operateRecord;

	@ApiModelProperty(value = "请求接口入参")
	private String constansRecord;

	@ApiModelProperty("操作人姓名")
	private String realName;

	@ApiModelProperty(value = "开始时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date logBeginTime;

	@ApiModelProperty(value = "结束时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date logEndTime;
}
