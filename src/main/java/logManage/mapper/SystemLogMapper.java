package logManage.mapper;

import cn.hutool.json.JSONObject;
import logManage.entity.SystemLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Title: LogMapper
 * Description: 
 * @author ggkt
 */
@Mapper
public interface SystemLogMapper {
	
	int insertLog(SystemLog systemLog);

    List<JSONObject> listPage(SystemLog systemLog);
}
