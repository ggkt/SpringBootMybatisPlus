package com.example.ggkt.common.controller;

import com.example.ggkt.common.entity.UploadResult;
import com.ggkt.common.utils.web.RestResult;
import config.annotation.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.time.LocalDate;

/**
 * 文件操作类
 * @author ggkt
 * @date 2021年06月17日
 */
@RestController
@RequestMapping("/fileUpload/*")
public class FileUploadController {
	private final static Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	@Value("${file.diskPath}")
    private String diskPath;
    @Value("${file.accessPath}")
    private String accessPath;

	/**
	 * 单文件上传
	 * @param file
	 * @return
	 */
	@Log("单文件上传")
	@PostMapping(value = "/doUploadFile")
	public RestResult doUploadFile(@RequestParam("file") MultipartFile file) {
		try {
			if (file.isEmpty()) {
				return RestResult.error("文件为空");
			}
			long size = file.getSize();
//			long limit = 30*1024*1024;
//			if (size>limit) {
//				return RestResult.error("文件最大限制为30M");
//			}
			// 获取文件名
			String originalName = file.getOriginalFilename();
			logger.info("-- 上传的文件名为：{}" , originalName);
			// 获取文件的后缀名
			String suffixName = originalName.substring(originalName.lastIndexOf("."));

			//文件名,不带后缀
			String name = originalName.substring(0, originalName.lastIndexOf("."));
			String fileName = originalName;
			LocalDate localDate = LocalDate.now();
			String datePath = localDate.getYear() + File.separator + localDate.getMonthValue() + File.separator+localDate.getDayOfMonth();
			String realFilePath = diskPath + File.separator +datePath + File.separator + fileName;
			File dest = new File(realFilePath);
			// 检测是否存在目录
			if (!dest.getParentFile().exists()) {
				dest.getParentFile().mkdirs();
			}
			//检测文件是否存在
			int fileIndex = 1;
			while (dest.exists()) {
				fileName = name+"("+fileIndex+")"+suffixName;
				realFilePath = diskPath + File.separator +datePath + File.separator + fileName;
				fileIndex++;
				dest = new File(realFilePath);
			}
			//保存到一个目标文件中
			file.transferTo(dest);
			return RestResult.success(accessPath+ File.separator +datePath + File.separator + fileName);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("上传文件失败",e);
		}
		return RestResult.error("文件上传失败");
	}

	@PostMapping(value = "/upload")
	public RestResult upload(@RequestParam("file") MultipartFile file) {
		try {
			if (file.isEmpty()) {
				return RestResult.error("文件为空");
			}
			long size = file.getSize();
			// 获取文件名
			String originalName = file.getOriginalFilename();
			logger.info("-- 上传的文件名为：{}" , originalName);
			// 获取文件的后缀名
			String suffixName = originalName.substring(originalName.lastIndexOf("."));
			//文件名,不带后缀
			String name = originalName.substring(0, originalName.lastIndexOf("."));
			String fileName = originalName;
			LocalDate localDate = LocalDate.now();
			String datePath = localDate.getYear() + File.separator + localDate.getMonthValue() + File.separator+localDate.getDayOfMonth();
			String realFilePath = diskPath + File.separator +datePath + File.separator + fileName;
			File dest = new File(realFilePath);
			// 检测是否存在目录
			if (!dest.getParentFile().exists()) {
				dest.getParentFile().mkdirs();
			}
			//检测文件是否存在
			int fileIndex = 1;
			while (dest.exists()) {
				fileName = name+"("+fileIndex+")"+suffixName;
				realFilePath = diskPath + File.separator +datePath + File.separator + fileName;
				fileIndex++;
				dest = new File(realFilePath);
			}
			//保存到一个目标文件中
			file.transferTo(dest);
			UploadResult entity= new UploadResult();
			entity.setFileName(fileName);
			entity.setRelativePath(File.separator +datePath + File.separator + fileName);
			entity.setWholePath(accessPath+ File.separator +datePath + File.separator + fileName);
			return RestResult.success(entity);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("上传文件失败",e);
		}
		return RestResult.error("文件上传失败");
	}

//	/**
//	 *删除文件
//	 * @param realPath
//	 * @return
//	 */
//	@Log("删除文件")
//	@RequestMapping(value = "/doRemoveFile")
//	public ResponseDto doRemoveFile(Integer fileId,String realPath) {
//		if (fileId==null || StringUtils.isBlank(realPath)) {
//			return ResponseDto.responseFail("请选择要删除的文件");
//		}
//		boolean result = false;
//		File file = null;
//        if (StringUtils.isNotBlank(realPath)) {
//            file = new File(realPath);
//        } else {
//            result = false;
//        }
//        //如果文件路径所对应的文件存在，并且是一个文件，则直接删除
//        if (file.exists() && file.isFile()) {
//            if (!file.delete()) {
//            	logger.error("文件删除失败");
//            }
//            result = true;
//        }
//        if(result) {
//        	return ResponseDto.responseOK("文件删除成功");
//        }else {
//        	return ResponseDto.responseFail("文件删除失败");
//        }
//    }
//
//	/**
//	 * 多文件上传
//	 * @param files
//	 * @return
//	 */
//	@Log("多文件上传")
//	@PostMapping(value = "/doUploadBatchFile")
//	public ResponseDto doUploadBatchFile(@RequestParam(required=false,value = "file") MultipartFile[] files) {
//		if (files==null || files.length<=0) {
//			return ResponseDto.responseFail("请选择文件");
//		}
//		List<FileInfo> fileInfoList = new ArrayList<>();
//		try {
//			for (MultipartFile file : files) {
//				if (file.isEmpty()) {
//					return ResponseDto.responseFail(file.getOriginalFilename() + "文件为空,请选择正确的文件");
//				}
//				// 获取文件名
//				String originalName = file.getOriginalFilename();
//				logger.info("-- 上传的文件名为：{}", originalName);
//				// 获取文件的后缀名
//				String suffixName = originalName.substring(originalName.lastIndexOf("."));
//
//				//文件名,不带后缀
//				String name = originalName.substring(0, originalName.lastIndexOf("."));
//				String fileName = originalName;
//				LocalDate localDate = LocalDate.now();
//				String datePath = localDate.getYear() + File.separator + localDate.getMonthValue() + File.separator + localDate.getDayOfMonth();
//				String realFilePath = diskPath + File.separator + datePath + File.separator + fileName;
//				File dest = new File(realFilePath);
//				// 检测是否存在目录
//				if (!dest.getParentFile().exists()) {
//					dest.getParentFile().mkdirs();
//				}
//				//检测文件是否存在
//				int fileIndex = 1;
//				while (dest.exists()) {
//					fileName = name + "(" + fileIndex + ")" + suffixName;
//					realFilePath = diskPath + File.separator + datePath + File.separator + fileName;
//					dest = new File(realFilePath);
//					fileIndex++;
//				}
//
//				//保存到一个目标文件中
//				file.transferTo(dest);
//				FileInfo fileInfo = new FileInfo();
//				fileInfo.setCreateUserId(getCurrentUserId());
//				fileInfo.setFileSize(dest.length());
//				fileInfo.setOriginalName(originalName);
//                fileInfo.setDownloadCount(0);
//                fileInfo.setViewCount(0);
//				fileInfo.setFileName(fileName);
//				fileInfo.setRealPath(realFilePath);
//				fileInfo.setSuffix(suffixName);
//				fileInfo.setAccessPath(accessPath+ File.separator +datePath + File.separator + fileName);
//				fileInfoList.add(fileInfo);
//
//			}
//			fileInfoService.insertList(fileInfoList);
//		} catch (Exception e) {
//			logger.error("上传文件失败",e);
//		}
//		return ResponseDto.responseOK(fileInfoList);
//	}
//
//	public ResponseEntity downLoad() {
//
//		return null;
//	}
}
