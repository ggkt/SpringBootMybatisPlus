package com.example.ggkt.common.entity;
import lombok.Data;
/**
 * @Author: ggkt
 * @Description: 文件上传结果对象
 * @Date: 2021/06/17
 * @Version: 1.0
 */
@Data
public class UploadResult {

    private String relativePath;
    private String fileName;
    private String wholePath;
}
