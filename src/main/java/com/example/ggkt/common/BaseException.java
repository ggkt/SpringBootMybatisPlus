package com.example.ggkt.common;

/**
 * <p>自定义基础业务异常</p>
 *
 * @author ggkt
 */
@SuppressWarnings("serial")
public class BaseException extends RuntimeException {

	/**
     * 数据库操作,insert返回0
     */
    public static final BaseException DB_INSERT_RESULT_0 = new BaseException("数据库操作,insert返回0");

    /**
     * 数据库操作,update返回0
     */
    public static final BaseException DB_UPDATE_RESULT_0 = new BaseException("数据库操作,update返回0");
    
    public BaseException() {
        super();
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    /**
     * 覆盖原来的，以提高异常性能
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }


}