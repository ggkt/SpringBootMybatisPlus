package com.example.ggkt.user.controller;

import com.example.ggkt.common.BaseException;
import com.example.ggkt.user.domain.Student;
import com.example.ggkt.user.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ggkt
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/student")
@Api(tags = "学生管理")
public class StudentController {
    @Autowired
    StudentService studentService;

    @ApiOperation("列表方法")
    @PostMapping("/list")
    public List<Student> list(){
        System.out.println(studentService.list().size());

        return studentService.list();
    }

    @ApiOperation("创建方法")
    @PostMapping("/create")
    public boolean create(@RequestBody Student entity){
        return studentService.saveOrUpdate(entity);
    }

    @ApiOperation("查看方法")
    @GetMapping("/view/{id}")
    public Student view(@PathVariable Integer id){
        if(id==null){
            throw new BaseException("111");
//            throw new RuntimeException("111");
        }
        return studentService.getById(id);
    }

    @ApiOperation("删除方法")
    @PostMapping("delete")
    public boolean delete(@RequestBody Student entity){
        if (entity==null || entity.getId()==null){
            System.out.println("参数错误");
            return false;
        }else{
            System.out.println("the id is:"+entity.getId());
        }
        return  studentService.removeById(entity.getId());
    }

}

