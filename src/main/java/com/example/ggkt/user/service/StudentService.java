package com.example.ggkt.user.service;

import com.example.ggkt.user.domain.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ggkt
 * @since 2021-05-24
 */
public interface StudentService extends IService<Student> {

}
