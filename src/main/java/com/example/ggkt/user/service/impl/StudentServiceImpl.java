package com.example.ggkt.user.service.impl;

import com.example.ggkt.user.domain.Student;
import com.example.ggkt.user.mapper.StudentMapper;
import com.example.ggkt.user.service.StudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ggkt
 * @since 2021-05-24
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

}
