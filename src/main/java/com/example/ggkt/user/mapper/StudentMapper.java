package com.example.ggkt.user.mapper;

import com.example.ggkt.user.domain.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ggkt
 * @since 2021-05-24
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {

}
